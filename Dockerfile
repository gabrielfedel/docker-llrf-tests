FROM registry.esss.lu.se/gabrielfedel/docker-e3:master

RUN ./e3.bash -t mod

WORKDIR /root/e3/e3-asyn
RUN git checkout 4.33.0
RUN make vars
RUN make init
RUN make build
RUN make install

WORKDIR /root/e3

RUN git clone https://github.com/icshwi/e3-loki
RUN make -C e3-loki vars
RUN make -C e3-loki init
RUN make -C e3-loki build
RUN make -C e3-loki install

RUN git clone https://github.com/icshwi/e3-nds
RUN make -C e3-nds vars
RUN make -C e3-nds init
RUN make -C e3-nds build
RUN make -C e3-nds install

RUN git clone https://github.com/icshwi/e3-scaling
RUN make -C e3-scaling vars
RUN make -C e3-scaling init
RUN make -C e3-scaling build
RUN make -C e3-scaling install

RUN yum install procServ pciutils -y 
RUN pip3 install pytest
